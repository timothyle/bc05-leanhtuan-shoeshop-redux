import { dataShoe } from "../../datashoeList";
import { ADD_TO_CART, CHANGE_DETAIL,UPDATE_QUANTITYUP,UPDATE_QUANTITYDOWN} from "../constantShoe/shoeConstant";

let initialState = {
    shoeArr: dataShoe,
    detail: dataShoe[1],
    cart : [],
  };
  
  export const shoeReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART:{
            let cloneCart =[...state.cart]
            let index = cloneCart.findIndex((item) => {
                  return item.id === action.payload.id;
                });
                if(index === -1){
                  let cartItem = {...action.payload,number:1};
                  cloneCart.push(cartItem);
                } else {
                  cloneCart[index].number++;
                }
                return {...state,cart:cloneCart}
        }
        case CHANGE_DETAIL: {
            state.detail = action.payload;
            return {...state}
        }
        case UPDATE_QUANTITYUP: {
          let cloneCart = [...state.cart];
          let index = cloneCart.findIndex(item => item.id === action.payload.id);
          cloneCart[index].number +=1;
          return {...state, cart:cloneCart}
        }
        case UPDATE_QUANTITYDOWN: {
          let cloneCart = [...state.cart];
          let index = cloneCart.findIndex(item => item.id === action.payload.id);
          if (cloneCart[index].number === 0){
            cloneCart.splice(index, 1)
          } else{
          cloneCart[index].number -= 1;
          }
          return {...state, cart:cloneCart}
        }
        default:
            return state;
    }
  };