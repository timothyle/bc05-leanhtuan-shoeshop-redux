import React, { Component } from 'react';
import { connect } from 'react-redux';
import { UPDATE_QUANTITYUP, UPDATE_QUANTITYDOWN } from "./redux/constantShoe/shoeConstant"


class Cart extends Component {
    renderTbody = () => { 
        return this.props.gioHangGiay.map((item) => {
          return (
            <tr>
              <td>{item.id}</td>
              <td>{item.name}</td>
              <td>${item.price * item.number}</td>
              <td>
                <button onClick={()=> {this.props.handleUpdateQuantityDOWN(item)}}>-</button>
                {item.number}
                <button onClick={()=> {this.props.handleUpdateQuantityUP(item)}}>+</button>
              </td>
              <td>
                <img style={{ width: "80px" }} src={item.image} alt="" />
              </td>
            </tr>
          );
        });
      };
      render() {
        return (
          <table className="table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Image</th>
              </tr>
            </thead>
            <tbody>{this.renderTbody()}</tbody>
          </table>
        );
      }
}
let mapStateToProps = (state) => {
  return {
    gioHangGiay: state.shoeReducer.cart,
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleUpdateQuantityUP:(shoe) =>{
    let action = {
      type: UPDATE_QUANTITYUP,
      payload: shoe
    }
    dispatch(action);
  },
  handleUpdateQuantityDOWN:(shoe) => {
    let action = {
      type: UPDATE_QUANTITYDOWN,
      payload: shoe
    }
    dispatch(action);
  }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Cart);